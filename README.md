# Anonymität mit Tor & Tails

Wir zeigen verschiedene Sachen:
* Threat models: Unterscheidung automatisierte & gezielte Überwachung, Staat oder Konzerne
* Wie viele Identitäten hast du (evtl Spiel ausdenken?)
* Disclaimer: Es gibt viele Bedrohungen für die Identität - und wir wissen eig nie, ob unsere Lösungen ausreichen. Aber: unsere Gegner sind nie perfekt, und man kann es versuchen. Auf jeden Fall: eigene Gedanken machen.
* Kategorien: Wird von Konzernen genutzt, wird von Staaten genutzt, ist technisch machbar.
* Generell gute Idee:
    * Hierarchiefreiheit verwirrt Cops
    * Gemeinschaftlich genutzte Rechner, Räume, Geräte, etc., durchtauschen -> Strafverfolgung ist individuell
    * Nur die Beteiligten müssen von etwas wissen; alle anderen sollten lernen, keine Fragen zu stellen
* Handys
    * Klarnamenpflicht seit 2017: anonyme Prepaid-Sims gibt es in vielen kleinen Handy-Shops
    * Nicht einfach in Handys tun, die ihr schon mit Klarnamen-Sims verwendet habt; IMEI
    * VDS
    * Funkzellenabfragen
    * Genaue Ortung: ist noch nicht entwickelt, aber wird kommen
    * Anrufe & SMSen sind komplett unverschlüsselt: Anrufe und SMSen dürfen nicht massengespeichert werden, aber wenn sie eure Nummer schon kennen, und was ahnen, dann dürfen sie mitschneiden.
    * In Gefahrengebieten: räumliche Überwachung & Mitschnitt von allem mit IMSI-Catchern möglich. Außerdem bessere räumliche Eingrenzung für Ortung
    * zu google-freien Aktionssmartphones: besser nur für Medien, ansonsten sind sie tendenziell leichter hackbar & sammeln mehr komische Daten.
* Veröffentlichungen
    * Netzbrummen: Sicherungen raus, anderen Ort wählen,
    * EXIF-Daten: auf Wire verschicken
    * Autorenanalyse: Schreibstil ändern, falsche Grammatik
* Surveillance Capitalism
    * Browser Fingerprinting: https://panopticlick.eff.org/
    * Kann man mit einem normalen Browser nicht so wirklich was dagegen machen; Methoden ändern sich ständig, wir wissen nicht, was die machen
    * uBlock + noScript + private Browsing hilft aber schon ein bisschen
* Tor
    * Browser
    * Standardisierung gegen Browser-Fingerprinting
    * Wie das Tor-Netzwerk funktioniert
    * Unterschied zu VPNs
    * don't maximize the window
* Tails
    * login
    * gnome erklären
    * stick clonen
    * neuen stick booten
    * encrypted persistent storage einrichten
    * in den Wartezeiten etwas aufs Darknet eingehen, ein paar Tipps einfließen lassen

## Requisiten

Aktueller Tails-Stick
Leerer USB-Stick

